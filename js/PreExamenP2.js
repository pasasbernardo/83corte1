async function buscarInformacion() {
    const countryName = document.getElementById('nameCountry').value;

    if (!countryName) {
        alert('Por favor, ingrese el nombre del país');
        return;
    }

    const apiUrl = `https://restcountries.com/v3.1/name/${countryName}`;

    try {
        const response = await fetch(apiUrl);

        if (!response.ok) {
            throw new Error('No se encontró información para el país ingresado');
        }

        const data = await response.json();
        mostrarInformacion(data[0]);
    } catch (error) {
        console.error('Error al realizar la petición:', error);
        alert(error.message);
    }
}

function mostrarInformacion(countryData) {
    document.getElementById('capital').value = countryData.capital || 'No disponible';

    const language = countryData.languages ? Object.values(countryData.languages) || 'No disponible' : 'No disponible';
    console.log(language)
    document.getElementById('language').value = language;
}

function limpiar() {
    document.getElementById('nameCountry').value = '';
    document.getElementById('capital').value = '';
    document.getElementById('language').value = '';
}
